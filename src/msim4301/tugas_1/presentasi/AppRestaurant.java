package msim4301.tugas_1.presentasi;

import java.util.Scanner;

public class AppRestaurant {

    private static MenuCategory mc;

    public static void main(String[] args) {

        // List Menu (Daftar Menu)
        Menu[] listMenu = new Menu[4];
        listMenu[0] = new Menu(mc.FOOD, "Fried Rice", new Double("15000"));
        listMenu[1] = new Menu(mc.FOOD, "Steak Wagyu A5", new Double("100000"));
        listMenu[2] = new Menu(mc.BEVERAGE, "Mineral Water", new Double("4000"));
        listMenu[3] = new Menu(mc.BEVERAGE, "Iced Tea", new Double("6000"));

        // Show listMenu (Tampilkan Daftar Menu)
        System.out.println("List Menu (Daftar Menu): ");
        System.out.println("1. " + listMenu[0].getMenuName() + " (Rp. " + listMenu[0].getMenuPrice() + ")");
        System.out.println("2. " + listMenu[1].getMenuName() + " (Rp. " + listMenu[1].getMenuPrice() + ")");
        System.out.println("3. " + listMenu[2].getMenuName() + " (Rp. " + listMenu[2].getMenuPrice() + ")");
        System.out.println("4. " + listMenu[3].getMenuName() + " (Rp. " + listMenu[3].getMenuPrice() + ")");

        // Proses
        Object[][] orderList = (Object[][]) doInput(listMenu);
        doCalculate(orderList);
    }

    private static void doCalculate(Object[][] orderList) {
        System.out.println("\nMy Order (Pesanan Saya): ");
        double total = new Double(0);
        int no = 1;

        //Proses 1. Hitung Menu 1
        int count1 = (int) orderList[0][1];
        if(count1 > 0) {
            String name1 = ((Menu)orderList[0][0]).getMenuName();
            double harga1 = ((Menu)orderList[0][0]).getMenuPrice();
            double jumlah1 = harga1 * count1;
            total = total + jumlah1;
            System.out.println(no++ + ". " + name1 + "\n@" + count1 + "pcs x Rp. " + harga1 + " = Rp. " + jumlah1 + "\n");
        }

        //Proses 2. Hitung Menu 2
        int count2 = (int) orderList[1][1];
        if(count2 > 0) {
            String name2 = ((Menu)orderList[1][0]).getMenuName();
            double harga2 = ((Menu)orderList[1][0]).getMenuPrice();
            double jumlah2 = harga2 * count2;
            total = total + jumlah2;
            System.out.println(no++ + ". " + name2 + "\n@" + count2 + "pcs x Rp. " + harga2 + " = Rp. " + jumlah2 + "\n");
        }

        //Proses 3. Hitung Menu 3
        int count3 = (int) orderList[2][1];
        if(count3 > 0) {
            String name3 = ((Menu)orderList[2][0]).getMenuName();
            double harga3 = ((Menu)orderList[2][0]).getMenuPrice();
            double jumlah3 = harga3 * count3;
            total = total + jumlah3;
            System.out.println(no++ + ". " + name3 + "\n@" + count3 + "pcs x Rp. " + harga3 + " = Rp. " + jumlah3 + "\n");
        }

        //Proses 4. Hitung Menu 4
        int count4 = (int) orderList[3][1];
        if(count4 > 0) {
            String name4 = ((Menu)orderList[3][0]).getMenuName();
            double harga4 = ((Menu)orderList[3][0]).getMenuPrice();
            double jumlah4 = harga4 * count4;
            total = total + jumlah4;
            System.out.println(no++ + ". " + name4 + "\n@" + count4 + "pcs x Rp. " + harga4 + " = Rp. " + jumlah4 + "\n");
        }

        //Proses 5. Hitung Total
        double pajak = total * 10/100;
        double biayaPelayanan = new Double(20000);
        double totalKeseluruhan = total + pajak + biayaPelayanan;
        boolean isDiskon = false;
        boolean isPromo = false;

        if(totalKeseluruhan > new Double(50000)) {
            isPromo = true;
        }

        if(totalKeseluruhan > new Double(100000)) {
            isDiskon = true;
            totalKeseluruhan = totalKeseluruhan - new Double(50000);
        }

        System.out.println("Struk Pembayaran : ");
        System.out.println("+Total Pesanan   = Rp. " + total);
        System.out.println("+Pajak (10%)     = Rp. " + pajak);
        System.out.println("+Biaya Pelayanan = Rp. " + biayaPelayanan);
        if(isDiskon) System.out.println("-Diskon          = Rp. 50000.0");
        System.out.println("=======TOTAL====== Rp. " + totalKeseluruhan);
        if(isPromo || isDiskon) System.out.println("\nSelamat!");
        if(isPromo) System.out.println("Karena transaksi Anda melebihi Rp. 50000.0, Anda mendapatkan penawaran beli satu gratis satu untuk salah satu kategori minuman!");
        if(isDiskon) System.out.println("Karena transaksi Anda melebihi Rp. 100000.0, Anda mendapatkan diskon sebesar Rp. 50000.0!");
    }

    private static Object doInput(Menu[] listMenu) {
        Scanner input = new Scanner(System.in);
        Object[][] orderList = new Object[4][2];

        System.out.println("\nInput Order (Mencatat Pesanan): ");
        System.out.print("1. " + listMenu[0].getMenuName() + " = ");
        int inputUser1 = input.nextInt();
        orderList[0][0] = listMenu[0];
        orderList[0][1] = inputUser1;

        System.out.print("2. " + listMenu[1].getMenuName() + " = ");
        int inputUser2 = input.nextInt();
        orderList[1][0] = listMenu[1];
        orderList[1][1] = inputUser2;

        System.out.print("3. " + listMenu[2].getMenuName() + " = ");
        int inputUser3 = input.nextInt();
        orderList[2][0] = listMenu[2];
        orderList[2][1] = inputUser3;

        System.out.print("4. " + listMenu[3].getMenuName() + " = ");
        int inputUser4 = input.nextInt();
        orderList[3][0] = listMenu[3];
        orderList[3][1] = inputUser4;

        return orderList;
    }

}
