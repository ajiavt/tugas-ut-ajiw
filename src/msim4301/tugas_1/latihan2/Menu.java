package msim4301.tugas_1.latihan2;

public class Menu {

    private MenuCategory category;
    private String menuName;
    private double menuPrice;

    public Menu(MenuCategory category, String menuName, double menuPrice) {
        this.category = category;
        this.menuName = menuName;
        this.menuPrice = menuPrice;
    }

    public MenuCategory getCategory() {
        return category;
    }

    public void setCategory(MenuCategory category) {
        this.category = category;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public double getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(double menuPrice) {
        this.menuPrice = menuPrice;
    }
}
