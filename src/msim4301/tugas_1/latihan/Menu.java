package msim4301.tugas_1.latihan;

 enum MenuCategory {
    MAKANAN,
    MINUMAN
}

public class Menu {

    private String kategori; // 0 - Makanan dan 1 - Minuman
    private String namaMenu;
    private double hargaMenu;

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public double getHargaMenu() {
        return hargaMenu;
    }

    public void setHargaMenu(double hargaMenu) {
        this.hargaMenu = hargaMenu;
    }

    public Menu(String kategori, String namaMenu, double hargaMenu) {
        this.kategori = kategori;
        this.namaMenu = namaMenu;
        this.hargaMenu = hargaMenu;
    }

}
