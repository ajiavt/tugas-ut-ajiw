package msim4301.tugas_1.latihan;
import java.util.Scanner;

public class AppRestoran {

    private static Integer maksimalPesanan = 4;
    private static Double pajak = (double) (0.1);
    private static Double biayaLayanan = 20000.0;


    public void engineTransaksi(Menu[] daftarPesanan) {
        double totalHarga = 0.0;
        int i = 0;
        boolean diskon10Persen = false;
        boolean beli1Gratis1Minuman = false;

        System.out.println("Detail Pesanan:");
        System.out.println("===================================");

        if(daftarPesanan[0] != null){
            Menu menu = daftarPesanan[0];
            System.out.println("Item 1: " + menu.getNamaMenu());
            System.out.println("Harga per Item 1: " + menu.getHargaMenu());
            System.out.println("----------------------------------\n");
            totalHarga += menu.getHargaMenu();
        }

        if(daftarPesanan[1] != null){
            Menu menu = daftarPesanan[1];
            System.out.println("Item 2: " + menu.getNamaMenu());
            System.out.println("Harga per Item 2: " + menu.getHargaMenu());
            System.out.println("----------------------------------\n");
            totalHarga += menu.getHargaMenu();
        }

        if(daftarPesanan[2] != null){
            Menu menu = daftarPesanan[2];
            System.out.println("Item 3: " + menu.getNamaMenu());
            System.out.println("Harga per Item 3: " + menu.getHargaMenu());
            System.out.println("----------------------------------\n");
            totalHarga += menu.getHargaMenu();
        }

        if(daftarPesanan[3] != null){
            Menu menu = daftarPesanan[3];
            System.out.println("Item 4: " + menu.getNamaMenu());
            System.out.println("Harga per Item 4: " + menu.getHargaMenu());
            System.out.println("----------------------------------\n");
            totalHarga += menu.getHargaMenu();
        }

        if(totalHarga > 100000){
            diskon10Persen = true;
        }

        if(totalHarga > 50000){
            beli1Gratis1Minuman = true;
        }

        double hargaSetelahPajak = totalHarga + (totalHarga * pajak);
        double hargaAkhir= hargaSetelahPajak + biayaLayanan;

        System.out.println("====================== STRUK PESANAN ======================\n");
        System.out.println("1. Total Biaya Pemesanan: " + totalHarga);
        System.out.println("2. Informasi Pajak (10%): Rp" + (totalHarga * pajak));
        System.out.println("3. Biaya Pelayanan: Rp" + biayaLayanan);
        System.out.print("4. Promo:");

        if(diskon10Persen){
            System.out.println("\n- Anda mendapatkan diskon sebesar 10% karena pesanan melebihi Rp100,000");
        }

        if(beli1Gratis1Minuman){
            System.out.println("\n- Anda mendapatkan penawaran beli satu gratis satu untuk minuman karena pesanan melebihi Rp50,000");
        }

        if(!diskon10Persen && !beli1Gratis1Minuman) {
            System.out.println("Tidak mendapatkan promo.");
        }

        System.out.println("\n5. Total yang harus dibayar: Rp" + hargaAkhir);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Daftar Menu
        Menu[] menu = new Menu[5];
        menu[0] = new Menu("makanan", "Nasi Goreng", 15000);
        menu[1] = new Menu("makanan","Mie Goreng", 18000);
        menu[2] = new Menu("makanan","Steak Wagyu A5", 100000);
        menu[3] = new Menu("minuman","Air Mineral", 4000);
        menu[4] = new Menu("minuman","Air Teh", 5000);

        // Tampilkan daftar menu
        System.out.println(" Daftar Menu:");
        System.out.println(" 1. " + menu[0].getNamaMenu() + " - Rp" + menu[0].getHargaMenu());
        System.out.println(" 2. " + menu[1].getNamaMenu() + " - Rp" + menu[1].getHargaMenu());
        System.out.println(" 3. " + menu[2].getNamaMenu() + " - Rp" + menu[2].getHargaMenu());
        System.out.println(" 4. " +menu [3].getNamaMenu() +" - Rp"+menu [3].getHargaMenu());
        System.out.println(" 5. " +menu [4].getNamaMenu() +" - Rp"+menu [4].getHargaMenu());

        // Daftar Pesanan
        Menu daftarPesanan[] = new Menu[maksimalPesanan];

        System.out.println(" Masukkan nomor pilihan makanan atau minuman (1-5), atau masukkan angka lain untuk selesai:");
        int pilihanUser1= input.nextInt();
        if(pilihanUser1>= 1 && pilihanUser1<= 4){
            daftarPesanan [0]=menu[pilihanUser1-1];
        }

        System.out.println(" Masukkan nomor pilihan makanan atau minuman (1-5), atau masukkan angka lain untuk selesai:");
        int pilihanUser2= input.nextInt();
        if(pilihanUser2>= 1 && pilihanUser2<= 4){
            daftarPesanan [1]=menu[pilihanUser2-1];
        }

        System.out.println(" Masukkan nomor pilihan makanan atau minuman (1-5), atau masukkan angka lain untuk selesai:");
        int pilihanUser3= input.nextInt();
        if(pilihanUser3>= 1 && pilihanUser3<= 4){
            daftarPesanan [2]=menu[pilihanUser3-1];
        }

        System.out.println(" Masukkan nomor pilihan makanan atau minuman (1-5), atau masukkan angka lain untuk selesai:");
        int pilihanUser4= input.nextInt();
        if(pilihanUser4>= 1 && pilihanUser4<= 4){
            daftarPesanan [3]=menu[pilihanUser4-1];
        }

        AppRestoran appRestoran=new AppRestoran();
        appRestoran.engineTransaksi(daftarPesanan);
    }

}
