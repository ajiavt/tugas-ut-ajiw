package msim4301.tugas_2.latihan;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public interface PemilikApp {

    static void main(Menu superMenu) {
        System.out.println();
        printHeader(superMenu);
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        int lastKey = ((TreeMap<Integer, Menu>) superMenu.getDaftarMenu()).lastKey();

        int lastId = lastKey + 1;
        do{
            if(input.equals("1")) {
                Menu newData = new Menu();

                System.out.print("PemilikApp - Mode Tambah Data - Kategori (1: Makanan, 2: Minuman): ");
                input = scanner.nextLine();
                newData.setKategori(input.equals("1") ? Kategori.Makanan : Kategori.Minuman);

                System.out.print("PemilikApp - Mode Tambah Data - Nama Menu: ");
                input = scanner.nextLine();
                newData.setNama(input);

                System.out.print("PemilikApp - Mode Tambah Data - Harga: ");
                input = scanner.nextLine();
                newData.setHarga(new Double(input));

                superMenu.getDaftarMenu().put(lastId++, newData);
                System.out.println("Data berhasil ditambahkan!");
            }

            else if(input.equals("2")) {
                System.out.print("PemilikApp - Mode Edit Data - ID Key: ");
                Integer key = Integer.parseInt(scanner.nextLine());
                Menu data = superMenu.getDaftarMenu().get(key);

                if(data == null) {
                    System.out.println("ID Key tidak ditemukan!");
                } else {
                    System.out.print("PemilikApp - Mode Edit Data - Ganti Harga " + data.getNama() + ": ");
                    input = scanner.nextLine();
                    data.setHarga(new Double(input));

                    System.out.print("PemilikApp - Mode Edit Data - Konfimasi - Anda yakin lakukan Ubah Harga? (ya/tidak): ");
                    String konfirmasi = scanner.nextLine();
                    if(konfirmasi.equalsIgnoreCase("ya")) {
                        superMenu.getDaftarMenu().put(key, data);
                    } else {
                        System.out.println("Gagal!");
                    }
                }
            }

            else if(input.equals("3")) {
                System.out.print("PemilikApp - Mode Hapus Data - ID Key: ");
                Integer key = Integer.parseInt(scanner.nextLine());

                System.out.print("PemilikApp - Mode Hapus Data - Konfimasi - Anda yakin hapus data ini? (ya/tidak): ");
                String konfirmasi = scanner.nextLine();

                if(konfirmasi.equalsIgnoreCase("ya")) {
                    superMenu.getDaftarMenu().remove(key);
                } else {
                    System.out.println("Gagal!");
                }
            }

            printHeader(superMenu);
            input = scanner.nextLine();
        } while(!input.trim().equalsIgnoreCase("selesai"));
    }

    static void printHeader(Menu menu) {
        menu.printDaftarMenuForAdmin();
        System.out.println("PemilikApp - (1: Tambah menu, 2: Ubah menu, 3: Hapus menu, selesai: Kembali)");
        System.out.print("PemilikApp - Pilihan saya: ");
    }
}
