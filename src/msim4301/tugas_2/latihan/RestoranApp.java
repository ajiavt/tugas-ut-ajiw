package msim4301.tugas_2.latihan;

import java.util.Scanner;

public class RestoranApp implements PemilikApp, PelangganApp{

    public static void main(String[] args) {
        Menu menu = new Menu();

        printTitle();
        Scanner s = new Scanner(System.in);
        String input = s.nextLine();

        do {
            if (input.equals("1")) {
                PelangganApp.main(menu);
            } else if (input.equals("2")) {
                PemilikApp.main(menu);
            } else if(input.trim().equalsIgnoreCase("selesai")){
                break;
            }

            printTitle();
            input = s.nextLine();
        } while (!input.trim().equalsIgnoreCase("selesai"));

        System.out.println("\nTerimakasih sudah menggunakan Aplikasi Aji Resto!");

    }

    public static void printTitle() {
        System.out.println("\n========= Aplikasi Aji Resto ========");
        System.out.format("|%-7s | %-21s\t| \n", "  Ketik", "Keterangan");
        System.out.println("=====================================");
        System.out.format("|%-7s | %-21s\t| \n", "      1", "Pesan Makanan/Minuman");
        System.out.format("|%-7s | %-21s\t| \n", "      2", "Kelola Menu");
        System.out.format("|%-7s | %-21s\t| \n", "selesai", "Keluar dari aplikasi");
        System.out.println("=====================================");
        System.out.print("RestoranApp - Pilihan saya: ");
    }
}
