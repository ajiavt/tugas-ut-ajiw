package msim4301.tugas_2.latihan;

import java.util.*;

public interface PelangganApp {


    static void main(Menu menu) {
        System.out.println();
        menu.printDaftarMenu();

        ArrayList<Menu> pesananSaya = new ArrayList();

        int jumlahPesanan = 1;
        System.out.print("PelangganApp - Pesanan ke-1: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        // Input
        do {
            for (Map.Entry<Integer, Menu> entry : menu.getDaftarMenu().entrySet()) {
                Integer key = entry.getKey();
                Menu newPesanan = entry.getValue();

                if(String.valueOf(key).equals(input.trim())) {
                    pesananSaya.add(newPesanan);
                    jumlahPesanan++;
                }

                else if(input.trim().equalsIgnoreCase("selesai")){
                    break;
                }
            }

            System.out.print("PelangganApp - Pesanan ke-" + jumlahPesanan + ": ");
            input = scanner.nextLine();
        } while (!input.trim().equalsIgnoreCase("selesai"));

        hitungPesanan(pesananSaya);
    }

    static void hitungPesanan(ArrayList<Menu> pesananSaya) {
        Map<String, Integer> jumlahItem = new HashMap<>();
        Map<String, Double> totalHarga = new HashMap<>();

        for (Menu pesanan : pesananSaya) {
            String nama = pesanan.getNama();
            Double harga = pesanan.getHarga();

            jumlahItem.put(nama, jumlahItem.getOrDefault(nama, 0) + 1);
            totalHarga.put(nama, totalHarga.getOrDefault(nama, 0.0) + harga);
        }

        System.out.println("\n====================== Struk Pembayaran =======================");
        System.out.println("| Nama           | Jumlah Item | Harga per Item | Total Harga |");
        System.out.println("===============================================================");

        Double totalBayar = new Double(0);
        for (String nama : jumlahItem.keySet()) {
            Double hargaItem = totalHarga.get(nama) / jumlahItem.get(nama);
            System.out.format("| %-14s | %11d |     %10.1f |  %10.1f |\n", nama, jumlahItem.get(nama), hargaItem, totalHarga.get(nama));
            totalBayar = totalBayar + totalHarga.get(nama);
        }

        boolean isPromo = false;
        boolean isDiskon = false;
        Double totalDiskon = new Double(0);
        if(totalBayar > new Double(50000)) {
            isPromo = true;
        }

        if(totalBayar > new Double(100000)) {
            isDiskon = true;
            totalDiskon = totalBayar * 10 / 100;
            totalBayar = totalBayar - totalDiskon;
        }

        Double biayaPelayanan = new Double(20000);
        Double finalBayar = totalBayar + biayaPelayanan + (totalBayar * 10 / 100);
        System.out.println("===============================================================");
        System.out.println("|                                   Total Bayar = Rp. " + (totalBayar + totalDiskon));
        if(isDiskon) System.out.println("|                                      - Diskon = Rp. " + totalDiskon);
        if(isDiskon) System.out.println("|                    Total Bayar Setelah Diskon = Rp. " + totalBayar);
        System.out.println("|                                   + Pajak (%) = 10%");
        System.out.println("|                                 + Biaya Pajak = Rp. " + (totalBayar * 10 / 100));
        System.out.println("|                             + Biaya Pelayanan = Rp. " + biayaPelayanan);
        System.out.println("|   Total Bayar + Biaya Pajak + Biaya Pelayanan = Rp. " + finalBayar);
        System.out.println("===============================================================");
        if(isPromo) System.out.println("*) Transaksi melebihi Rp. 50000.0, Anda mendapatkan penawaran beli satu gratis satu untuk salah satu kategori minuman!");
        if(isDiskon) System.out.println("*) Transaksi melebihi Rp. 100000.0, Anda mendapatkan diskon sebesar 10% atau Rp. " + totalDiskon + "!");
    }
}
