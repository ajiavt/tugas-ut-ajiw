package msim4301.uas;

public class MenghitungRata {
    public static void main(String[] args){
        double nilai1 = 3;
        double nilai2 = 5;
        double nilai3 = 2;
        double result = (nilai1 + nilai2 + nilai3) / 3;
        result = Math.round(result * 100.0) / 100.0; // Pembulatan 2 angka di belakang koma

        System.out.println("Hasil Rata-rata adalah : " + result);
    }
}
