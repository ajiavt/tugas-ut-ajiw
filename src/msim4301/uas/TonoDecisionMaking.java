package msim4301.uas;

public class TonoDecisionMaking {
    public static void main(String[] args) {
        int nilaiTono = 80;
        String PT = "";

        if(nilaiTono > 81) {
            PT = "PT A";
        } else if(nilaiTono >= 71 && nilaiTono <= 80) {
            PT = "PT B";
        } else if(nilaiTono >= 61 && nilaiTono <= 70) {
            PT = "PT C";
        } else {
            PT = "PT D";
        }

        System.out.println("Tono cocok masuk Perguruan Tinggi: " + PT);
    }
}
