package msim4301.tugas_3;

public class Makanan extends MenuItem {
    private String jenisMakanan;

    public Makanan(String nama, double harga, String jenisMakanan) {
        super(nama, harga, "Makanan");
        this.jenisMakanan = jenisMakanan;
    }

    // Implementasi metode tampilMenu untuk Makanan
    @Override
    public void tampilMenu() {
        System.out.println(nama + " (" + jenisMakanan + ") - Rp" + harga);
    }

    public String getJenisMakanan() {
        return jenisMakanan;
    }
}
