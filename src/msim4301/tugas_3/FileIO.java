package msim4301.tugas_3;

import java.io.*;
import java.util.ArrayList;

import java.io.*;
import java.util.ArrayList;

public class FileIO {
    // Metode untuk menyimpan daftar menu ke file teks
    public static void simpanMenuKeFile(String path, Menu menu) {
        try (PrintWriter writer = new PrintWriter(path)) {
            for (MenuItem item : menu.getDaftarMenu()) {
                writer.println(item.getClass().getSimpleName() + " | " +
                        item.getNama() + " | " +
                        item.getHarga() + " | " +
                        (item instanceof Makanan ? ((Makanan) item).getJenisMakanan() :
                                (item instanceof Minuman ? ((Minuman) item).getJenisMinuman() :
                                        (item instanceof Diskon ? String.valueOf(((Diskon) item).getDiskon()) : ""))));
            }
            System.out.println("Daftar menu berhasil disimpan ke file: " + path);
        } catch (IOException e) {
            System.out.println("Gagal menyimpan daftar menu ke file: " + path);
            e.printStackTrace();
        }
    }

    // Metode untuk memuat daftar menu dari file teks
    public static Menu muatMenuDariFile(String path) {
        Menu menu = new Menu();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] data = line.split(" \\| ");
                if (data.length >= 4) {
                    String kategori = data[0];
                    String nama = data[1];
                    double harga = Double.parseDouble(data[2]);

                    switch (kategori) {
                        case "Makanan":
                            menu.tambahMenu(new Makanan(nama, harga, data[3]));
                            break;
                        case "Minuman":
                            menu.tambahMenu(new Minuman(nama, harga, data[3]));
                            break;
                        case "Diskon":
                            menu.tambahMenu(new Diskon(nama, harga, Double.parseDouble(data[3])));
                            break;
                        default:
                            System.out.println("Kategori tidak valid: " + kategori);
                    }
                }
            }
            System.out.println("Daftar menu berhasil dimuat dari file: " + path);
        } catch (IOException e) {
            System.out.println("Gagal memuat daftar menu dari file: " + path);
            e.printStackTrace();
        }
        return menu;
    }

    // Metode untuk mengekspor struk pesanan ke file teks
    public static void eksporStrukKeFile(String path, Pesanan pesanan) {
        try (PrintWriter writer = new PrintWriter(path)) {
            writer.println("===== Struk Pesanan =====");
            for (MenuItem item : pesanan.getPesananPelanggan()) {
                writer.println(item.getClass().getSimpleName() + " | " +
                        item.getNama() + " | " +
                        item.getHarga() + " | " +
                        (item instanceof Makanan ? ((Makanan) item).getJenisMakanan() :
                                (item instanceof Minuman ? ((Minuman) item).getJenisMinuman() :
                                        (item instanceof Diskon ? String.valueOf(((Diskon) item).getDiskon()) : ""))));
            }
            writer.println("Total: Rp" + pesanan.hitungTotal());
            writer.println("=========================");
            System.out.println("Struk pesanan berhasil diekspor ke file: " + path);
        } catch (IOException e) {
            System.out.println("Gagal mengekspor struk pesanan ke file: " + path);
            e.printStackTrace();
        }
    }
}
