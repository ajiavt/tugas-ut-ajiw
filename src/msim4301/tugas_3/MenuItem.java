package msim4301.tugas_3;

public abstract class MenuItem {
    protected String nama;
    protected double harga;
    protected String kategori;

    public MenuItem(String nama, double harga, String kategori) {
        this.nama = nama;
        this.harga = harga;
        this.kategori = kategori;
    }

    // Metode abstrak untuk menampilkan informasi menu
    public abstract void tampilMenu();

    public double getHarga() {
        return harga;
    }

    public String getNama() {
        return nama;
    }
}