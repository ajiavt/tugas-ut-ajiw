package msim4301.tugas_3;

import java.util.ArrayList;

public class Menu {
    private ArrayList<MenuItem> daftarMenu = new ArrayList<>();

    // Metode untuk menambahkan item baru ke menu
    public void tambahMenu(MenuItem item) {
        daftarMenu.add(item);
    }

    // Metode untuk menampilkan menu restoran
    public void tampilMenuRestoran() {
        System.out.println("===== Menu Restoran =====");
        for (MenuItem item : daftarMenu) {
            item.tampilMenu();
        }
        System.out.println("=========================");
    }

    // Metode untuk mencari item berdasarkan nama
    public MenuItem cariItem(String nama) {
        for (MenuItem item : daftarMenu) {
            if (item.getNama().equalsIgnoreCase(nama)) {
                return item;
            }
        }
        return null;
    }

    // Metode untuk mendapatkan daftar menu
    public ArrayList<MenuItem> getDaftarMenu() {
        return daftarMenu;
    }
}
