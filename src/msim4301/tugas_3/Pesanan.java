package msim4301.tugas_3;

import java.util.ArrayList;

public class Pesanan {
    private ArrayList<MenuItem> pesananPelanggan = new ArrayList<>();

    // Metode untuk menambahkan item pesanan
    public void tambahPesanan(MenuItem item) {
        pesananPelanggan.add(item);
    }

    // Metode untuk menghitung total biaya pesanan
    public double hitungTotal() {
        double total = 0;
        for (MenuItem item : pesananPelanggan) {
            if (item instanceof Diskon) {
                // Jika item adalah diskon, kurangkan total dengan nilai diskon
                Diskon diskonItem = (Diskon) item;
                total -= diskonItem.getDiskon();
            } else {
                total += item.getHarga();
            }
        }
        return total;
    }

    // Metode untuk menampilkan struk pesanan pelanggan
    public void tampilStruk() {
        System.out.println("===== Struk Pesanan =====");
        for (MenuItem item : pesananPelanggan) {
            item.tampilMenu();
        }
        System.out.println("Total: Rp" + hitungTotal());
        System.out.println("=========================");
    }

    // Metode untuk mendapatkan daftar pesanan pelanggan
    public ArrayList<MenuItem> getPesananPelanggan() {
        return pesananPelanggan;
    }
}
