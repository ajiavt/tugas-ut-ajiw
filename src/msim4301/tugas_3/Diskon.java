package msim4301.tugas_3;

public class Diskon extends MenuItem {
    private double diskon;

    public Diskon(String nama, double harga, double diskon) {
        super(nama, harga, "Diskon");
        this.diskon = diskon;
    }

    // Implementasi metode tampilMenu untuk Diskon
    @Override
    public void tampilMenu() {
        System.out.println(nama + " - Diskon Rp" + diskon);
    }

    public double getDiskon() {
        return diskon;
    }

    public void setDiskon(double diskon) {
        this.diskon = diskon;
    }
}
