package msim4301.tugas_3;

public class Minuman extends MenuItem {
    private String jenisMinuman;

    public Minuman(String nama, double harga, String jenisMinuman) {
        super(nama, harga, "Minuman");
        this.jenisMinuman = jenisMinuman;
    }

    // Implementasi metode tampilMenu untuk Minuman
    @Override
    public void tampilMenu() {
        System.out.println(nama + " (" + jenisMinuman + ") - Rp" + harga);
    }

    public String getJenisMinuman() {
        return jenisMinuman;
    }
}
