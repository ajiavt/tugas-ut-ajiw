package msim4301.tugas_3;
import java.util.InputMismatchException;
import java.util.Scanner;

public class RestoranApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Menu menuRestoran = new Menu();
        Pesanan pesananPelanggan = new Pesanan();

        // Contoh menambahkan item ke menu restoran
        menuRestoran.tambahMenu(new Makanan("Nasi Goreng", 25000, "Makanan Utama"));
        menuRestoran.tambahMenu(new Minuman("Teh Leci", 5000, "Teh"));
        menuRestoran.tambahMenu(new Minuman("Teh Jasmine", 6000, "Teh"));
        menuRestoran.tambahMenu(new Diskon("Promo 1", 0, 10000));

        int pilihan;
        do {
            // Menu utama
            System.out.println("\n===== Menu Utama =====");
            System.out.println("1. Tambah Item Menu");
            System.out.println("2. Tampilkan Menu Restoran");
            System.out.println("3. Pesan Menu");
            System.out.println("4. Hitung Total Biaya Pesanan");
            System.out.println("5. Tampilkan Struk Pesanan");
            System.out.println("6. Cetak Struk ke File");
            System.out.println("7. Import Daftar Menu dari File");
            System.out.println("8. Keluar");

            try {
                System.out.print("Pilih menu: ");
                pilihan = scanner.nextInt();

                switch (pilihan) {
                    case 1:
                        // Menambah item baru ke menu restoran
                        System.out.print("Nama item: ");
                        scanner.nextLine();  // Membersihkan buffer
                        String namaItem = scanner.nextLine();
                        System.out.print("Harga: Rp");
                        double hargaItem = scanner.nextDouble();
                        System.out.print("Kategori (Makanan/Minuman/Diskon): ");
                        scanner.nextLine();  // Membersihkan buffer
                        String kategoriItem = scanner.nextLine();

                        MenuItem newItem;
                        if (kategoriItem.equalsIgnoreCase("Makanan")) {
                            System.out.print("Jenis Makanan: ");
                            String jenisMakanan = scanner.nextLine();
                            newItem = new Makanan(namaItem, hargaItem, jenisMakanan);
                        } else if (kategoriItem.equalsIgnoreCase("Minuman")) {
                            System.out.print("Jenis Minuman: ");
                            String jenisMinuman = scanner.nextLine();
                            newItem = new Minuman(namaItem, hargaItem, jenisMinuman);
                        } else if (kategoriItem.equalsIgnoreCase("Diskon")) {
                            System.out.print("Persentase Diskon (%): ");
                            double persentaseDiskon = scanner.nextDouble();
                            newItem = new Diskon(namaItem, hargaItem, persentaseDiskon);
                        } else {
                            System.out.println("Kategori tidak valid!");
                            continue;
                        }

                        menuRestoran.tambahMenu(newItem);
                        System.out.println("Item berhasil ditambahkan ke menu.");
                        break;

                    case 2:
                        // Menampilkan menu restoran
                        menuRestoran.tampilMenuRestoran();
                        break;

                    case 3:
                        // Menerima pesanan pelanggan
                        System.out.print("Masukkan nama item yang ingin dipesan: ");
                        scanner.nextLine();  // Membersihkan buffer
                        String itemPesan = scanner.nextLine();
                        MenuItem pesananItem = menuRestoran.cariItem(itemPesan);

                        if (pesananItem != null) {
                            pesananPelanggan.tambahPesanan(pesananItem);
                            System.out.println("Pesanan berhasil ditambahkan.");
                        } else {
                            System.out.println("Item tidak ditemukan dalam menu.");
                        }
                        break;

                    case 4:
                        // Menghitung total biaya pesanan dengan mempertimbangkan diskon
                        double totalBiaya = pesananPelanggan.hitungTotal();
                        System.out.println("Total Biaya Pesanan: Rp" + totalBiaya);
                        break;

                    case 5:
                        // Menampilkan struk pesanan pelanggan
                        pesananPelanggan.tampilStruk();
                        break;

                    case 6:
                        System.out.print("Masukkan path untuk menyimpan struk (termasuk nama file): ");
                        scanner.nextLine();  // Membersihkan buffer
                        String pathStruk = scanner.nextLine();
                        FileIO.eksporStrukKeFile(pathStruk, pesananPelanggan);
                        System.out.println("Struk berhasil disimpan di " + pathStruk);
                        break;

                    case 7:
                        System.out.print("Masukkan path file untuk mengimport daftar menu: ");
                        scanner.nextLine();  // Membersihkan buffer
                        String pathMenu = scanner.nextLine();
                        Menu menuImport = FileIO.muatMenuDariFile(pathMenu);
                        if (menuImport != null) {
                            menuRestoran = menuImport;
                            System.out.println("Daftar menu berhasil diimport dari " + pathMenu);
                        } else {
                            System.out.println("Gagal mengimport daftar menu dari " + pathMenu);
                        }
                        break;

                    case 8:
                        // Keluar dari program
                        System.out.println("Terima kasih telah menggunakan aplikasi restoran!");
                        break;

                    default:
                        System.out.println("Pilihan tidak valid. Silakan coba lagi.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Masukan tidak valid. Harap masukkan angka.");
                scanner.nextLine();  // Membersihkan buffer
                pilihan = 0;
            }
        } while (pilihan != 8);

        scanner.close();
    }
}