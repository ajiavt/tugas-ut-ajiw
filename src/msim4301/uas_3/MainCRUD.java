package msim4301.uas_3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class MainCRUD {

    static final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
    static final String DB_URL = "jdbc:mariadb://localhost:3306/db_uas";
    static final String USER = "root";
    static final String PASS = "";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("======= MENU UTAMA =======");
        System.out.println("1. Input Data");
        System.out.println("2. Tampil Data");
        System.out.println("3. Update Data");
        System.out.println("0. Keluar");

        System.out.print("PILIHAN> ");
        int pilihan = scanner.nextInt();
        if (pilihan == 1) {
            try {
                Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);

                System.out.print("Masukkan nama: ");
                String nama = scanner.next();
                System.out.print("Masukkan alamat: ");
                String alamat = scanner.next();

                // Pernyataan SQL untuk insert data
                String sql = "INSERT INTO tabel_mahasiswa (nama, alamat) VALUES (?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, nama);
                preparedStatement.setString(2, alamat);

                int rowCount = preparedStatement.executeUpdate();
                if (rowCount > 0) {
                    System.out.println("Data berhasil ditambahkan!");
                } else {
                    System.out.println("Gagal menambahkan data.");
                }

                // Tutup koneksi
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (pilihan == 2) {
            try {
                Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);

                // Pernyataan SQL untuk select data
                String sql = "SELECT * FROM tabel_mahasiswa";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                ResultSet resultSet = preparedStatement.executeQuery();

                // Menampilkan hasil
                while (resultSet.next()) {
                    System.out.println("ID: " + resultSet.getInt("id"));
                    System.out.println("Nama: " + resultSet.getString("nama"));
                    System.out.println("Alamat: " + resultSet.getString("alamat"));
                    System.out.println("--------------");
                }

                // Tutup koneksi
                resultSet.close();
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (pilihan == 3) {
            try {
                Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);

                System.out.print("Masukkan ID yang akan diupdate: ");
                int idToUpdate = scanner.nextInt();
                System.out.print("Masukkan nama baru: ");
                String newNama = scanner.next();
                System.out.print("Masukkan alamat baru: ");
                String newAlamat = scanner.next();

                // Pernyataan SQL untuk update data berdasarkan ID
                String sql = "UPDATE tabel_mahasiswa SET nama = ?, alamat = ? WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, newNama);
                preparedStatement.setString(2, newAlamat);
                preparedStatement.setInt(3, idToUpdate);

                int rowCount = preparedStatement.executeUpdate();
                if (rowCount > 0) {
                    System.out.println("Data berhasil diupdate!");
                } else {
                    System.out.println("Gagal mengupdate data.");
                }

                // Tutup koneksi
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (pilihan == 0) {
            // Fitur Keluar
            System.out.println("Keluar dari program.");
        } else {
            System.out.println("Pilihan tidak ditemukan!");
        }
    }
}
