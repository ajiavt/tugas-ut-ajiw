package msim4301.uas_2;

public class Segitiga extends Matematika{

    private int alas;
    private int tinggi;

    public Segitiga(int alas, int tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
    }

    @Override
    public int luas() {
        return (this.alas * this.tinggi) / 2;
    }
}
