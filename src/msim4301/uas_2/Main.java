package msim4301.uas_2;

public class Main {
    public static void main(String[] args) {
        PersegiPanjang pp = new PersegiPanjang(5, 10);
        System.out.println("Luas dari Persegi Panjang adalah: " + pp.luas());

        Segitiga s = new Segitiga(10, 15);
        System.out.println("Luas dari Segitiga adalah: " + s.luas());
    }
}
