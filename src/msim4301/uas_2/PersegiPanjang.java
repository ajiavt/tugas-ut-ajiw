package msim4301.uas_2;

public class PersegiPanjang extends Matematika{

    private int panjang;
    private int lebar;

    public PersegiPanjang(int panjang, int lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    @Override
    public int luas() {
        return this.panjang * this.lebar;
    }
}
